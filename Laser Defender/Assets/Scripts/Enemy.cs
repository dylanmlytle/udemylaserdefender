﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public GameObject projectile;
    public float projectileSpeed;
    public float health;
    public float fireRate;
    public int scoreValue;

    public AudioClip fireSound;
    public AudioClip deathSound;

    private ScoreKeeper scoreKeeper;

    private void Start()
    {
        scoreKeeper = GameObject.Find("Score").GetComponent<ScoreKeeper>();
    }

    private void Update()
    {
        float probabilityOfFiringThisFrame = Time.deltaTime * fireRate;
        if (Random.value < probabilityOfFiringThisFrame)
        {
            Fire();
        }
        
    }

    private void Fire()
    { 
        GameObject missile = Instantiate(projectile, transform.position, Quaternion.identity) as GameObject;
        missile.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -projectileSpeed);
        AudioSource.PlayClipAtPoint(fireSound, transform.position);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Projectile missile = collision.gameObject.GetComponent<Projectile>();

        if(missile)
        {
            missile.Hit();
            health -= missile.GetDamage();
            if(health <= 0)
            {
                Die();
            }
            Debug.Log("Hit by projectile");
        }
    }

    void Die()
    {
        AudioSource.PlayClipAtPoint(fireSound, transform.position);
        Destroy(gameObject);
        scoreKeeper.Score(scoreValue);
    }
}
