﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {
    public GameObject enemyPrefab;
    public float width;
    public float height;
    public float speed;
    public float spawnDelay;

    private bool movingRight = false;

    private float xMin;
    private float xMax;

    
	// Use this for initialization
	void Start () {

        float distance = transform.position.z - Camera.main.transform.position.z;
        Vector3 leftMostPosition = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance));
        Vector3 rightMostPosition = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distance));
        xMin = leftMostPosition.x + (width * 0.5f);
        xMax = rightMostPosition.x - (width * 0.5f);

        SpawnUntilFull();
    }

    public void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(width, height));
    }

    // Update is called once per frame
    void Update () {
        if(movingRight)
        {
            transform.position += Vector3.right * speed * Time.deltaTime;
        }
        else
        {
            transform.position += Vector3.left * speed * Time.deltaTime;
        }

        if(transform.position.x <= xMin)
        {
            movingRight = true;
        }
        else if(transform.position.x >= xMax)
        {
            movingRight = false;
        }

        float newXPosition = Mathf.Clamp(transform.position.x, xMin, xMax);
        transform.position = new Vector3(newXPosition, transform.position.y);
        
        if(AllMembersDead())
        {
            SpawnUntilFull();
        }
        
    }

    Transform NextFreePosition()
    {
        foreach (Transform childPositionGameObject in transform)
        {
            if (childPositionGameObject.childCount == 0)
            {
                return childPositionGameObject;
            }
        }

        return null;
    }

    bool AllMembersDead()
    {
        foreach(Transform childPositionGameObject in transform)
        {
            if(childPositionGameObject.childCount > 0)
            {
                return false;
            }
        }

        return true;
    }

    void SpawnUntilFull()
    {
        Transform nextPosition = NextFreePosition();
        if (nextPosition)
        {
            GameObject enemy = Instantiate(enemyPrefab, nextPosition.position, Quaternion.identity) as GameObject;
            enemy.transform.parent = nextPosition;

            Invoke("SpawnUntilFull", spawnDelay);
        }

        
    }
}
