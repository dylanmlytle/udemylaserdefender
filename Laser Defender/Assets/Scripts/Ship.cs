﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour {

    public float shipMovementPerFrame;
    public float padding;
    public GameObject projectile;
    public float projectileSpeed;
    public float fireSpeed;
    public float health;
    public AudioClip fireSound;

    float xMin;
    float xMax;

	// Use this for initialization
	void Start ()
    {
        float distance = transform.position.z - Camera.main.transform.position.z;
        Vector3 leftMostPosition = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance));
        Vector3 rightMostPosition = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distance));
        xMin = leftMostPosition.x + padding;
        xMax = rightMostPosition.x - padding;
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            InvokeRepeating("Fire", 0.00000001f, fireSpeed * Time.deltaTime);
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            CancelInvoke("Fire");
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position += Vector3.left * shipMovementPerFrame * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.position += Vector3.right * shipMovementPerFrame * Time.deltaTime;
        }

        float newXPosition = Mathf.Clamp(transform.position.x, xMin, xMax);
        transform.position = new Vector3(newXPosition, transform.position.y);
    }

    void Fire()
    {
        Vector3 offset = new Vector3(0, 1, 0);
        GameObject beam = Instantiate(projectile, transform.position + offset, Quaternion.identity) as GameObject;
        beam.GetComponent<Rigidbody2D>().velocity = new Vector3(0, projectileSpeed, 0);
        AudioSource.PlayClipAtPoint(fireSound, transform.position);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Projectile missile = collision.gameObject.GetComponent<Projectile>();

        if (missile)
        {
            missile.Hit();
            health -= missile.GetDamage();
            if (health <= 0)
            {
                Die();
            }
            Debug.Log("Hit by projectile");
        }
    }

    private void Die()
    {
        LevelManager levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        levelManager.LoadLevel("Win Screen");
        Destroy(gameObject);
    }
}
